//
//  server.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"


int
answerToClient(int client)
{
    FILE *fp = NULL;
	RSA* rsa;
    struct crypto msg;
    int ret = -1, i = 0, digest_len = 20, key_len = 0;
    char *digest, mydigest[digest_len];
    unsigned char* key;
    unsigned char* enckey;
    uint32_t enckey_len;
    char *file_pem = "priv.pem";
    char *priv_key_pass = "password";
    char priv_hash_pass[20];
  
	//Ricevo la chiave di sessione dal client
	receiveMessage(client, (char **) &enckey, &enckey_len);
	
	//Allocazione del contesto di RSA
	OpenSSL_add_all_algorithms();
    rsa = RSA_new();  
    
    //calcolo la chiave con cui aprire priv.pem
    hash_sha1(priv_key_pass, strlen(priv_key_pass), priv_hash_pass);
	
	//Leggo la chiave privata dal file
	fp = fopen(file_pem, "r");
	rsa = PEM_read_RSAPrivateKey(fp,&rsa,NULL,priv_hash_pass);
	fclose(fp);
	
	//Decifro la chiave di sessione con la chiave privata
	key = (unsigned char*) calloc(1,RSA_size(rsa));
	key_len = RSA_private_decrypt(enckey_len,enckey,key,rsa,RSA_PKCS1_PADDING);
	
    //Inserisco la chiave nella struttura
    stpncpy(msg.k,(char *)key,8);
	
    //Ricevo il cipher text dal client
    receiveMessage(client, &msg.ct, &msg.ct_len);
	
    printf("\nCiphertext size: %u\n",msg.ct_len);
    
    //decifro il messaggio
    my_decrypt(&msg);
    
    
    // salvo il digest ricevuto
    digest = &msg.pt[msg.pt_len - digest_len];
    msg.pt_len -= digest_len;    
    
    //Stampo il digest ricevuto
    printf("\nDigest received with the message:\nSHA-1 =>  ");
	
    for (i = 0; i < digest_len ; i++)
	printbyte(digest[i]);
    
    printf("\n\nDigest computed on the message:");
    
    // calcolo il digest sul testo ricevuto
    hash_sha1(msg.pt,msg.pt_len,mydigest);

    //Confronto i digest
    ret = memcmp(digest,mydigest,digest_len);
    
    if(ret == 0)
    {
	printf("Message verified!\n");
        
        //Stampo il messaggio a video
        //printf("\nPlain text:\n%s\n\n", msg.pt);
        
        //Salvo il messaggio nel file "received-data.txt"
        fp = fopen("received-data.txt", "w");
        if (fp == NULL)
        {
            perror("Opening file error\n");
            exit(EXIT_FAILURE);
        }
        
        fprintf(fp, "%s", msg.pt);
        fclose(fp);
    }
    else
	printf("Message NOT verified!\n\n");
    

    
    //Deallocazione messaggio e chiave di cifratura
    free(key);
    free(msg.pt);
    free(msg.ct);
    RSA_free(rsa);
	
    return 0;
}


int
main(void)
{
	//allocazione delle strutture dati necessarie
	struct sockaddr_in my_addr, cl_addr;
	int ret, len, sk, cn_sk;
	char ip[20];
	
	//Verbose
	printf("Starting server...\n");
	
	//creazione del socket di ascolto
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sk == -1)
	{
		perror("Errore nella creazione del socket");
		return 0;
	}
	
	//inizializzazione delle strutture dati
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(1234);
	
	//bind del socket
	ret = bind(sk, (SA *) &my_addr, sizeof(my_addr));
	
	if(ret == -1)
	{
		perror("Errore nell'esecuzione della bind del socket");
		return 0;
	}
    
	//messa in ascolto del server sul socket
	ret = listen(sk, 10);
	
	if(ret == -1)
	{
		perror("Errore nella messa in ascolto del server");
		return 0;
	}
	
	//dimensioni della struttura dove viene salvato l'ind del client
	len = sizeof(cl_addr);
	
	//ciclo in cui il server accetta connessioni in ingresso e le gestisce
	for(;;)
	{
		uint16_t port = 0;
		
		//Azzero le strutture dati
		memset(&cl_addr, 0, sizeof(cl_addr));
		memset(&ip, 0, 20);
		
        printf("\nWaiting for client message...\n\n");
        
		//accettazione delle connessioni ingresso
		cn_sk = accept(sk, (SA *) &cl_addr, (socklen_t *) &len);
		inet_ntop(AF_INET,&cl_addr.sin_addr,ip,20);
		port = ntohs(cl_addr.sin_port);
		
		if(cn_sk == -1)
		{
			perror("Errore nell'accettazione di una richiesta");
			return 0;
		}
		else
			printf("Accepted request from %s:%hu\n",ip,port);
		
		//gestione delle richieste
		answerToClient(cn_sk);
	}
	
	//Chiusura del socket di ascolto
	//Questo codice non verrà mai eseguito dato che si trova
	//dopo un ciclo infinito
	
	printf("\nClosing server...\n");
	close(sk);
	return 0;
}



